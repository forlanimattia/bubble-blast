package com.company.model;

public class Ball {

    private int stato, posizioneX, posizioneY, sommaStati;
    private String simbolo;

    public Ball(int stato, int posizioneX, int posizioneY) {
        setStato(stato);
        this.posizioneX = posizioneX;
        this.posizioneY = posizioneY;
    }

    public Ball(int stato, int posizioneX, int posizioneY, int sommaStati) {
        setStato(stato);
        this.posizioneX = posizioneX;
        this.posizioneY = posizioneY;
        this.sommaStati = sommaStati;
    }

    public Ball(int stato) {
        this.stato = stato;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public int getSommaStati() {
        return sommaStati;
    }

    public void setSommaStati(int sommaStati) {
        this.sommaStati = sommaStati;
    }

    public int getStato() {
        return stato;
    }

    public void setStato(int stato) {
        if (stato == 0){
            this.simbolo=".";
        }else if (stato == 1) {
            this.simbolo="◌";
        }else if (stato == 2) {
            this.simbolo="●";
        }else if (stato == 3) {
            this.simbolo="☼";
        }
        this.stato = stato;
    }

    public int getPosizioneX() {
        return posizioneX;
    }

    public void setPosizioneX(int posizioneX) {
        this.posizioneX = posizioneX;
    }

    public int getPosizioneY() {
        return posizioneY;
    }

    public void setPosizioneY(int posizioneY) {
        this.posizioneY = posizioneY;
    }



    @Override
    public String toString() {
        return  ""+stato;
    }
}

package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class Player {

    String username;
    List<Move> moves;
    List<Ball[][]> tables;

    public Player(String username) {
        this.username = username;
    }

    public Player(String username, List<Move> moves) {
        this.username = username;
        this.moves = moves;
    }
    public void addTable(Ball[][] table){
        if (tables == null){
            tables = new ArrayList<>();
        }
        tables.add(table);
    }

    public List<Ball[][]> getTables() {
        return tables;
    }

    public void setTables(List<Ball[][]> tables) {
        this.tables = tables;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public void setMoves(List<Move> moves) {
        this.moves = moves;
    }

    @Override
    public String toString() {
        return "Player: "  + username;
    }

    public void addMove(int x, int y){
        if (moves == null){
            moves = new ArrayList<>();
        }
        moves.add(new Move(x,y));
    }
}

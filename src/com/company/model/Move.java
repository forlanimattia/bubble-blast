package com.company.model;

public class Move {

    private int x;
    private int y;
    private String description;

    public Move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Move(int x, int y, String description) {
        this.x = x;
        this.y = y;
        this.description = description;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Move: " + "x= " + x +
                ", y= " + y +
                ", description= '" + description + "'\n";
    }
}

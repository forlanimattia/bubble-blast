package com.company.util;

        import com.company.model.Ball;
        import com.company.model.Move;

        import java.io.IOException;
        import java.lang.Math;
        import java.nio.file.Files;
        import java.nio.file.Path;
        import java.nio.file.Paths;
        import java.util.*;

public class Utilities {

    public static void fillGrid(Ball[][] grid){

        Ball b = null;
        for (int i=0; i<grid.length;i++){
            for (int j=0; j< grid[i].length;j++){
                int rand = (int)(Math.random() * 4);
                b= new Ball(rand,i,j);
                grid[i][j] = b;
            }
        }
    }

    public static void checkHorizontal(Ball[][] grid, int x, int y){
        for (int i=y; i<grid[x].length; i++){
            if(checkPosition(grid, new Move(x,i)))
                break;
        }
        for (int i=y; i>=0; i--){
            if (checkPosition(grid,new Move(x,i)))
                break;
        }
    }

    public static void checkVertical(Ball[][] grid, int x, int y){

        for (int i=x; i< grid.length; i++){
            if(checkPosition(grid, new Move(i,y)))
                break;
        }
        for (int i=x; i>=0; i--){
            if (checkPosition(grid,new Move(i,y)))
                break;
        }

    }

    public static void printGrid(Ball[][] grid){
        System.out.println("   0 1 2 3 4" );

        Arrays.stream(grid).forEach((i) -> {
            Arrays.stream(i).forEach((j) -> {
                if (j.getPosizioneY() == 0){
                    System.out.print(j.getPosizioneX() + "  ");
                }
                System.out.print(j.getSimbolo() + " ");
            });
            System.out.println();
        });
    }

    public static boolean checkPosition(Ball[][] grid, Move move){
        if (grid[move.getX()][move.getY()].getStato()!=0){
            alterBall(grid, move);
            return true;
        }else move.setDescription("sbagliato");
        return false;
    }

    public static boolean alterBall(Ball[][] grid, Move move){
        if( grid[move.getX()][move.getY()].getStato() != 0){
            if(grid[move.getX()][move.getY()].getStato() == 3){
                grid[move.getX()][move.getY()].setStato(0);
                move.setDescription("palla scoppiata");
                checkVertical(grid, move.getX(), move.getY());
                checkHorizontal(grid, move.getX(),move.getY());
            }else{
                String operation = grid[move.getX()][move.getY()].getStato() == 1 ? "palla gonfia" : "sta per scoppiare";
                move.setDescription(operation);
                grid[move.getX()][move.getY()].setStato(grid[move.getX()][move.getY()].getStato()+1);
            }

            return true;
        }
        return false;
    }

    public static boolean endGame(Ball[][] grid){

        return Arrays.asList(grid).stream()
                .allMatch(a->Arrays.asList(a).stream()
                        .allMatch(r->r.getStato() == 0));
    }

    public static void fileText(List<String> a){

        try {
            Path path = Paths.get("C:/Users/Mattia.Forlani/Desktop/test.txt");
            Files.write(path, a);
        }catch (IOException e){
            e.printStackTrace();
        }

    }

    public static List<String> createPrint(List<Ball[][]> tables, List<Move> moves){
        List<String> list = new ArrayList<>();
        for (int i =0;i< moves.size();i++){
            list.add(createPrintMove(moves.get(i)));
            for (int y =0;y<tables.get(i).length;y++){
                list.add(createPrintRowTable(tables.get(i)[y]));
            }
        }
        return list;
    }

    public static String createPrintMove(Move m){

        return "Mossa "+m.getX()+" "+m.getY()+" "+m.getDescription();
    }

    public static String createPrintRowTable(Ball[] ball){
        String row ="";
        for (int i=0;i<ball.length;i++){
            row+=ball[i].getSimbolo()+" ";
        }
        return row;

    }

    public static Ball[][] fillTestGrid(Ball[][] grid){
        Ball[][] a = new Ball[6][5];
        for (int i =0;i<grid.length;i++){
            for (int y=0; y<grid[i].length;y++){
                Ball b = grid[i][y];
                a[i][y]=new Ball(b.getStato(),b.getPosizioneX(),b.getPosizioneY());
            }
        }
        return a;
    }
}

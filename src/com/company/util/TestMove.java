package com.company.util;

        import com.company.model.Ball;
        import com.company.model.Move;

        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.Comparator;
        import java.util.List;

public class TestMove {

    private  Ball[][] gridTest;

    public TestMove(Ball[][] griglia) {
        this.gridTest =new Ball[6][5];
        fillTestGrid(griglia);
    }

    public void fillTestGrid(Ball[][] griglia){

        for (int i =0;i<griglia.length;i++){
            for (int y=0; y<griglia[i].length;y++){
                Ball b = griglia[i][y];
                gridTest[i][y]=new Ball(b.getStato(),b.getPosizioneX(),b.getPosizioneY());
            }
        }

    }

    public int numMoves(){
        int count =0;
        do{
            Ball b = bestChoice();
            Utilities.checkPosition(gridTest,new Move(b.getPosizioneX(),b.getPosizioneY()));
            count++;
        }while (!Utilities.endGame(gridTest));
        return count;
    }

    public static List<Ball> findValue(Ball[][] grid, int value){
        List<Ball> list= new ArrayList<>();
        Arrays.stream(grid).forEach(r->
                Arrays.stream(r).forEach(p-> {
                    if (p.getStato()==value)
                        list.add(p);
                }));
        return  list;
    }

    public  Ball bestChoice(){
        List<Ball> list = findValue(gridTest, 3);
        //Ball max= list.get(0);

        if (list.isEmpty()){
            list = findValue(gridTest, 2);
        }else if(list.isEmpty()){
            list = findValue(gridTest, 1);
        }
        return list.stream().max(Comparator.comparing(this::countsAround)).get();

       /* for (int i=1;i<list.size();i++){
            if (countsAround(list.get(i)) > countsAround(max)){
                max = list.get(i);
            }
        }
        return max;*/
    }

    public  int countsAround(Ball b){

        return countRight(b,0,0)+
                countUp(b,0,0)+
                countLeft(b,0,0)+
                countDown(b,0,0)+
                b.getStato();
    }

    public  int countRight(Ball b,int sum,int count){

        for (int i = b.getPosizioneY(); i < gridTest[b.getPosizioneX()].length; i++){
            if(i != b.getPosizioneY() && gridTest[b.getPosizioneX()][i].getStato()!=0){
                sum += gridTest[b.getPosizioneX()][i].getStato();
                count++;
                if(count<=1){
                    sum = countRight(gridTest[b.getPosizioneX()][i],sum,count);
                    sum = countUp(gridTest[b.getPosizioneX()][i],sum,count);
                    sum = countDown(gridTest[b.getPosizioneX()][i],sum,count);
                }
                break;
            }
        }
        return sum;
    }

    public  int countLeft(Ball b, int sum, int count){
        for (int i = b.getPosizioneY(); i >=0; i--){
            if(i != b.getPosizioneY() && gridTest[b.getPosizioneX()][i].getStato()!=0){
                sum += gridTest[b.getPosizioneX()][i].getStato();
                count++;
                if(count<=1){
                    sum = countLeft(gridTest[b.getPosizioneX()][i],sum,count);
                    sum = countUp(gridTest[b.getPosizioneX()][i],sum,count);
                    sum = countDown(gridTest[b.getPosizioneX()][i],sum,count);
                }
                break;
            }
        }
        return sum;
    }

    public  int countUp(Ball b, int sum, int count){
        for (int i = b.getPosizioneX(); i >=0; i--){
            if(i != b.getPosizioneX() && gridTest[i][b.getPosizioneY()].getStato()!=0){
                sum += gridTest[i][b.getPosizioneY()].getStato();
                count++;
                if(count<=1){
                    sum = countUp(gridTest[i][b.getPosizioneY()],sum,count);
                    sum = countRight(gridTest[i][b.getPosizioneY()],sum,count);
                    sum = countLeft(gridTest[i][b.getPosizioneY()],sum,count);
                }
                break;
            }
        }
        return sum;
    }

    public  int countDown(Ball b, int sum, int count){
        for (int i = b.getPosizioneX(); i < gridTest.length; i++){
            if(i != b.getPosizioneX() && gridTest[i][b.getPosizioneY()].getStato()!=0){
                sum += gridTest[i][b.getPosizioneY()].getStato();
                count++;
                if(count<=1){
                    sum = countDown(gridTest[i][b.getPosizioneY()],sum,count);
                    sum = countRight(gridTest[i][b.getPosizioneY()],sum,count);
                    sum = countLeft(gridTest[i][b.getPosizioneY()],sum,count);
                }
                break;
            }
        }
        return sum;
    }

}

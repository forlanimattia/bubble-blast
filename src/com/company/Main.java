package com.company;

import com.company.model.Ball;
import com.company.model.Player;
import com.company.util.TestMove;
import com.company.util.Utilities;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n" +
                " ________      ___  ___      ________      ________      ___           _______      \n" +
                "|\\   __  \\    |\\  \\|\\  \\    |\\   __  \\    |\\   __  \\    |\\  \\         |\\  ___ \\     \n" +
                "\\ \\  \\|\\ /_   \\ \\  \\\\\\  \\   \\ \\  \\|\\ /_   \\ \\  \\|\\ /_   \\ \\  \\        \\ \\   __/|    \n" +
                " \\ \\   __  \\   \\ \\  \\\\\\  \\   \\ \\   __  \\   \\ \\   __  \\   \\ \\  \\        \\ \\  \\_|/__  \n" +
                "  \\ \\  \\|\\  \\   \\ \\  \\\\\\  \\   \\ \\  \\|\\  \\   \\ \\  \\|\\  \\   \\ \\  \\____    \\ \\  \\_|\\ \\ \n" +
                "   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\\n" +
                "    \\|_______|    \\|_______|    \\|_______|    \\|_______|    \\|_______|    \\|_______|\n" +
                "                                                                                    \n" +
                "                                                                                    \n" +
                "                                                                                    \n" +
                " ________      ___           ________      ________       _________                 \n" +
                "|\\   __  \\    |\\  \\         |\\   __  \\    |\\   ____\\     |\\___   ___\\               \n" +
                "\\ \\  \\|\\ /_   \\ \\  \\        \\ \\  \\|\\  \\   \\ \\  \\___|_    \\|___ \\  \\_|               \n" +
                " \\ \\   __  \\   \\ \\  \\        \\ \\   __  \\   \\ \\_____  \\        \\ \\  \\                \n" +
                "  \\ \\  \\|\\  \\   \\ \\  \\____    \\ \\  \\ \\  \\   \\|____|\\  \\        \\ \\  \\               \n" +
                "   \\ \\_______\\   \\ \\_______\\   \\ \\__\\ \\__\\    ____\\_\\  \\        \\ \\__\\              \n" +
                "    \\|_______|    \\|_______|    \\|__|\\|__|   |\\_________\\        \\|__|              \n" +
                "                                             \\|_________|                           \n" +
                "                                                                                    \n" +
                "                                                                                    \n");
        Ball[][] griglia = new Ball[6][5];
        Utilities.fillGrid(griglia);
        TestMove t = new TestMove(griglia);
        int max=t.numMoves();
        System.out.println("mosse massime: " + max);
        Utilities.printGrid(griglia);

        System.out.println("Inserire Username");
        Player p1 = new Player(sc.next());
        System.out.println(p1);
        p1.addTable(Utilities.fillTestGrid(griglia));

        Utilities.printGrid(griglia);

        do{
            int x, y;
            do {
                System.out.println("seleziona la bolla");
                x = sc.nextInt();
                y = sc.nextInt();
                System.out.println(x > 5 || y > 4 ? "inserire coordinate corrette" : "");

            }while (x > 5 || y > 4);
            p1.addMove(x,y);
            Utilities.checkPosition(griglia, p1.getMoves().get(p1.getMoves().size()-1));
            Utilities.printGrid(griglia);
            p1.addTable(Utilities.fillTestGrid(griglia));

        }while(max > p1.getMoves().size() && !Utilities.endGame(griglia));
        System.out.println(Utilities.endGame(griglia) ?  "hai vinto" : "Hai perso" );
        Utilities.fileText(Utilities.createPrint(p1.getTables(),p1.getMoves()));
    }


}
